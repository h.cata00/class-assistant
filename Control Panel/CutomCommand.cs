﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Panel
{
    public partial class CutomCommand : Form
    {
        public bool ok = false;
        public string cmd = "";
        public CutomCommand()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //cmd.Insert(3, "nimica");
            cmd = "<cmd-ui>"+textBox1.Text;
            ok = true;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cmd = "<cmd-srv>" + textBox1.Text;
            ok = true;
            this.Close();
        }
    }
}
