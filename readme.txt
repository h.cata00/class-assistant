Pentru realizarea acestui proect au fost folosite urmatoarele librarii si fragmente de cod:

Notificari de tip toast
     * https://github.com/noxad/windows-toast-notifications


Listare ferestre deschise:
     * https://stackoverflow.com/questions/7268302/get-the-titles-of-all-open-windows
     * https://stackoverflow.com/questions/17868453/find-and-close-multiple-windows


Verificare si schimbare task manager:
     * https://stackoverflow.com/questions/16610567/enable-disable-taskmanager

Preluare stare actuala ferestre:
     * https://stackoverflow.com/questions/1003073/how-to-check-whether-another-app-is-minimized-or-not