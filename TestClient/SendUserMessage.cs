﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestClient
{
    public partial class SendUserMessage : Form
    {
        public string message = "";
        public bool ok = false;
        public SendUserMessage()
        {
            InitializeComponent();
        }
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hwnd, int message, int lparam, int wparam);

        [DllImport("user32.dll")]
        public static extern void ReleaseCapture();

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, 0xA1, 0x2, 0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            message = textBox1.Text;
            ok = true;
            this.Close();
        }
    }
}
