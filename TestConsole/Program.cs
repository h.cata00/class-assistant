﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            int timeout = 10;
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            int wantedPort = 6161;    //this is the port you want
            string prefix = GetIPAddress().Substring(0, GetIPAddress().LastIndexOf(".")) + ".";
            bool connected = false;
            while(!connected)
            {
                for (int i = 100; i <= 255; i++)
                {
                    socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    IAsyncResult result = socket.BeginConnect(prefix + i, wantedPort, null, null);

                    bool success = result.AsyncWaitHandle.WaitOne(300, true);

                    if (socket.Connected)
                    {
                        socket.EndConnect(result);
                        Console.WriteLine("found: " + prefix + i);
                        Console.ReadKey();
                        connected = true;
                    }
                    else
                    {
                        // NOTE, MUST CLOSE THE SOCKET

                        socket.Close();
                        Console.WriteLine("not working: " + prefix + i);
                        //throw new ApplicationException("Failed to connect server.");
                    }
                }
                timeout += 10;
            }
            

        }

        public static string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
    }
}
